package ru.konovalov.tm.exeption.empty;


import ru.konovalov.tm.exeption.AbstractException;

public class EmptyIdException extends AbstractException {

    public EmptyIdException() {
        super("Error. Id is empty");
    }

    public EmptyIdException(String value) {
        super("Error. " + value + " id is empty");
    }

}
