package ru.konovalov.tm.exeption.user;

import ru.konovalov.tm.exeption.AbstractException;

public class EmailExistsException extends AbstractException {

    public EmailExistsException() {
        super("Error! Email is already in use...");
    }

}
