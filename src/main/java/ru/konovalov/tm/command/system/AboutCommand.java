package ru.konovalov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.konovalov.tm.command.AbstractCommand;

public final class AboutCommand extends AbstractCommand {

    @NotNull
    @Override
    public final String arg() {
        return "-a";
    }

    @NotNull
    @Override
    public final String name() {
        return "about";
    }

    @NotNull
    @Override
    public final String description() {
        return "Show developer info";
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("Petr Konovalov");
        System.out.println("pkonovalov@tsconsulting.com");
    }
}
