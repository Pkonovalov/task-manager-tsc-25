package ru.konovalov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.konovalov.tm.command.AbstractCommand;

public final class VersionCommand extends AbstractCommand {

    @NotNull
    @Override
    public final String arg() {
        return "-v";
    }

    @NotNull
    @Override
    public String name() {
        return "version";
    }

    @NotNull
    @Override
    public String description() {
        return "Show version";
    }

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println(serviceLocator.getPropertyService().getApplicationVersion());
    }

}
