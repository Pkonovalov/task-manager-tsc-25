package ru.konovalov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.konovalov.tm.command.AbstractCommand;

import java.util.Collection;

import static ru.konovalov.tm.util.ValidationUtil.isEmpty;

public final class ArgumentsCommand extends AbstractCommand {

    @NotNull
    @Override
    public final String arg() {
        return "-a";
    }

    @NotNull
    @Override
    public final String name() {
        return "arguments";
    }

    @NotNull
    @Override
    public final String description() {
        return "Show all arguments";
    }

    @Override
    public void execute() {
        System.out.println("[ARGUMENTS]");
        final Collection<AbstractCommand> arguments = serviceLocator.getCommandService().getArguments();
        for (final AbstractCommand argument : arguments) {
            final String arg = argument.arg();
            if (isEmpty(arg)) continue;
            System.out.println(arg);
        }
    }
}
