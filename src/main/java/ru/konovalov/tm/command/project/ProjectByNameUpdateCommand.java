package ru.konovalov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.konovalov.tm.exeption.empty.EmptyNameException;
import ru.konovalov.tm.exeption.entity.ProjectNotFoundException;
import ru.konovalov.tm.util.TerminalUtil;

import static ru.konovalov.tm.util.ValidationUtil.isEmpty;

public final class ProjectByNameUpdateCommand extends AbstractProjectCommand {

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "project-update-by-name";
    }

    @Override
    public @NotNull String description() {
        return "Update project by name";
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[UPDATE PROJECT]");
        System.out.println("[ENTER NAME:]");
        final String name = TerminalUtil.nextLine();
        if (!serviceLocator.getProjectService().existsByName(userId, name)) throw new ProjectNotFoundException();
        System.out.println("ENTER NEW NAME:");
        final String nameNew = TerminalUtil.nextLine();
        if (isEmpty(nameNew)) throw new EmptyNameException();
        System.out.println("ENTER DESCRIPTION:");
        serviceLocator.getProjectService().updateProjectByName(userId, name, nameNew, TerminalUtil.nextLine());
    }

}
