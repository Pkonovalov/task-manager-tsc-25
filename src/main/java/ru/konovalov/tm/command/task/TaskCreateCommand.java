package ru.konovalov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.konovalov.tm.exeption.empty.EmptyNameException;
import ru.konovalov.tm.model.Task;
import ru.konovalov.tm.util.TerminalUtil;

import static ru.konovalov.tm.util.ValidationUtil.isEmpty;

public final class TaskCreateCommand extends AbstractTaskCommand {
    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-create";
    }

    @NotNull
    @Override
    public String description() {
        return "Create task";
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        if (isEmpty(name)) throw new EmptyNameException();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Task task = add(name, description);
        serviceLocator.getTaskService().add(userId, task);
    }

}
